  /**
 * HC-SR04 Demo
 * Demonstration of the HC-SR04 Ultrasonic Sensor
 * Date: August 3, 2016
 * 
 * Description:
 *  Connect the ultrasonic sensor to the Arduino as per the
 *  hardware connections below. Run the sketch and open a serial
 *  monitor. The distance read from the sensor will be displayed
 *  in centimeters and inches.
 * 
 * Hardware Connections:
 *  Arduino | HC-SR04 
 *  -------------------
 *    5V    |   VCC     
 *    7     |   Trig     
 *    8     |   Echo     
 *    GND   |   GND
 *  
 * License:
 *  Public Domain
 */

#include <FastLED.h>

// Pins
const int TRIG_PIN = 7;
const int ECHO_PIN = 8;

//Number of LEDs
#define NUM_LEDS 9

#define WARN_DISTANCE 24 

//Define our clock and data lines
#define DATA_PIN 11
#define CLOCK_PIN 13

//Create the LED array
CRGB leds[NUM_LEDS];


// Anything over 400 cm (23200 us pulse) is "out of range"
const unsigned int MAX_DIST = 23200;

void setup() {

  // The Trigger pin will tell the sensor to range find
  pinMode(TRIG_PIN, OUTPUT);
  digitalWrite(TRIG_PIN, LOW);

  // We'll use the serial monitor to view the sensor output
  Serial.begin(9600);

  
  //Tell FastLED what we're using. Note "BGR" where you might normally find "RGB".
  //This is just to rearrange the order to make all the colors work right.
  FastLED.addLeds<APA102, DATA_PIN, CLOCK_PIN, BGR>(leds, NUM_LEDS);

  //Set global brightness
  FastLED.setBrightness(50);
}

void loop() {

  unsigned long t1;
  unsigned long t2;
  unsigned long pulse_width;
  float cm;
  float bumperDistance;
  int setpoint;
  int loopcount = 0;
  int i;
  CRGB ringColor;
  CRGB innerColor;
  // Hold the trigger pin high for at least 10 us
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);

  // Wait for pulse on echo pin
  while ( digitalRead(ECHO_PIN) == 0 );

  // Measure how long the echo pin was held high (pulse width)
  // Note: the micros() counter will overflow after ~70 min
  t1 = micros();
  while ( digitalRead(ECHO_PIN) == 1);
  t2 = micros();
  pulse_width = t2 - t1;

  // Calculate distance in centimeters and inches. The constants
  // are found in the datasheet, and calculated from the assumed speed 
  //of sound in air at sea level (~340 m/s).
  cm = pulse_width / 58.0;
  bumperDistance = pulse_width / 148.0;

  // Print out results
  if ( pulse_width > MAX_DIST ) {
    Serial.println("Out of range");
  } else {
    Serial.print(bumperDistance);
    Serial.print(" in \n");
    setpoint = analogRead(A0)/10;
  // print out the value you read:
    Serial.println(setpoint);

    if(bumperDistance < setpoint)
    {
       innerColor = CRGB::Red;
       ringColor = CRGB::Red;
    }
    else if ((bumperDistance > setpoint + WARN_DISTANCE * 0) && (bumperDistance < setpoint + WARN_DISTANCE * 1))
    {
      innerColor = CRGB::Red;
      ringColor = CRGB::Yellow;
    }
    else if ((bumperDistance > setpoint + WARN_DISTANCE * 1) && (bumperDistance < setpoint + WARN_DISTANCE * 2))
    {
      innerColor = CRGB::Yellow;
      ringColor = CRGB::Yellow;
    }
    else if ((bumperDistance > setpoint + WARN_DISTANCE * 2) && (bumperDistance < setpoint + WARN_DISTANCE * 3))
    {
      innerColor = CRGB::Yellow;
      ringColor = CRGB::Green;
    }
    else if ((bumperDistance > setpoint + WARN_DISTANCE * 3))
    {
      innerColor = CRGB::Green;
      ringColor = CRGB::Green;
    }


    for(i = 0; i < NUM_LEDS; i++)
    {
      leds[i] = ringColor;
    }
    leds[4] = innerColor;
    FastLED.show();
  }
  
  // Wait at least 60ms before next measurement
  delay(60);
}
